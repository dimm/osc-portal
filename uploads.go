package main

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

func resourcesHandler(w http.ResponseWriter, r *http.Request) {
	session, err := filestore.Get(r, "osc-session")
	if err != nil || session.IsNew {
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}
	t, _ := template.New("main_layout.tmpl").
		ParseFiles("template/main_layout.tmpl", "template/resources.tmpl")

	resData := CreateIndexData(session)

	t.Execute(w, resData)
}

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	session, err := filestore.Get(r, "osc-session")
	if err != nil || session.IsNew {
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}

	t, _ := template.ParseFiles("template/main_layout.tmpl", "template/upload.tmpl")
	t.Execute(w, CreateIndexData(session))
}

func uploadFileHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		fmt.Printf("Not post")
		http.Redirect(w, r, "/", http.StatusSeeOther)
		return
	}

	session, err := filestore.Get(r, "osc-session")
	if err != nil || session.IsNew {
		http.Redirect(w, r, "/", http.StatusSeeOther)
	}

	if manifest, _, err := r.FormFile("manifest"); err == nil {
		defer manifest.Close()

		if data, err := ioutil.ReadAll(manifest); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "Error reading file %v", err)
			return
		} else {
			if css, err := ParseChecksum(data); err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				fmt.Fprintf(w, "Error parsing file %v", err)
				return
			} else {

				var user User
				oscdb.Where("subject = ?", session.Values["id"].(string)).First(&user)

				for _, cs := range css {
					oscdb.Create(&Resource{
						Name: cs.Filename,
						UserID: user.ID,
						Checksum: fmt.Sprintf("%x", cs.Checksum),
						Type: cs.Type,
					})
				}

				w.WriteHeader(http.StatusCreated)
			}
			return
		}
	} else if file, fileHandle, err := r.FormFile("file"); err == nil {
		defer file.Close()

		if data, err := ioutil.ReadAll(file); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Fprintf(w, "Error reading file %v", err)
			return
		} else {
			h := sha256.New()
			if _, err := io.Copy(h, bytes.NewReader(data)); err != nil {
				log.Fatal(err)
			}

			var user User
			oscdb.Where("subject = ?", session.Values["id"].(string)).First(&user)

			oscdb.Create(&Resource{
				Name: fileHandle.Filename,
				UserID: user.ID,
				Checksum: fmt.Sprintf("%x", h.Sum(nil) ),
				Type: "SHA256",
			})

			w.WriteHeader(http.StatusCreated)
			return
		}

	} else {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "No file or manifest in the request")
		return
	}

}