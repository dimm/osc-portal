name: "example.com"

# Describe what the target network is/does.
description: "The network which will host my first blockchain"

# Schema version of the content. Used by the SDK to apply the corresponding parsing rules.
version: 1.0.0

# The client section used by GO SDK.
client:
  # Which organization does this application instance belong to? The value must be the name of an org
  organization: org1
  logging:
    level: info

  # Root of the MSP directories with keys and certs. The Membership Service Providers is component that aims to offer an abstraction of a membership operation architecture.
  cryptoconfig:
    path: "/home/fabric/fabric_1.4/fabric-samples/basic-network/crypto-config"

  # Some SDKs support pluggable KV stores, the properties under "credentialStore" are implementation specific
  credentialStore:
    path: "/tmp/osc-kvs"

    # [Optional]. Specific to the CryptoSuite implementation used by GO SDK. Software-based implementations requiring a key store. PKCS#11 based implementations does not.
    cryptoStore:
      path: "/tmp/osc-msp"

  # BCCSP config for the client. Used by GO SDK. It's the Blockchain Cryptographic Service Provider.
  # It offers the implementation of cryptographic standards and algorithms.
  BCCSP:
    security:
      enabled: true
      default:
        provider: "SW"
      hashAlgorithm: "SHA2"
      softVerify: true
      level: 256

  tlsCerts:
    systemCertPool: false

# [Optional]. But most apps would have this section so that channel objects can be constructed based on the content below.
# If one of your application is creating channels, you might not use this
channels:
  mychannel:
    orderers:
      - orderer.example.com

    # Network entity which maintains a ledger and runs chaincode containers in order to perform operations to the ledger. Peers are owned and maintained by members.
    peers:
      peer0.org1.example.com:
        # [Optional]. will this peer be sent transaction proposals for endorsement? The peer must
        # have the chaincode installed. The app can also use this property to decide which peers
        # to send the chaincode install request. Default: true
        endorsingPeer: true

        # [Optional]. will this peer be sent query proposals? The peer must have the chaincode
        # installed. The app can also use this property to decide which peers to send the
        # chaincode install request. Default: true
        chaincodeQuery: true

        # [Optional]. will this peer be sent query proposals that do not require chaincodes, like
        # queryBlock(), queryTransaction(), etc. Default: true
        ledgerQuery: true

        # [Optional]. will this peer be the target of the SDK's listener registration? All peers can
        # produce events but the app typically only needs to connect to one to listen to events.
        # Default: true
        eventSource: true

    policies:
      #[Optional] options for retrieving channel configuration blocks
      queryChannelConfig:
        #[Optional] min number of success responses (from targets/peers)
        minResponses: 1
        #[Optional] channel config will be retrieved for these number of random targets
        maxTargets: 1
        #[Optional] retry options for query config block
        retryOpts:
          #[Optional] number of retry attempts
          attempts: 5
          #[Optional] the back off interval for the first retry attempt
          initialBackoff: 500ms
          #[Optional] the maximum back off interval for any retry attempt
          maxBackoff: 5s
          #[Optional] he factor by which the initial back off period is exponentially incremented
          backoffFactor: 2.0

# List of participating organizations in this network
organizations:
  org1:
    mspid: Org1MSP
    cryptoPath: "peerOrganizations/org1.example.com/users/{userName}@org1.example.com/msp"
    peers:
      - peer0.org1.example.com
        #certificateAuthorities:
        #- ca.example.com

# List of orderers to send transaction and channel create/update requests to.
# The orderers consent on the order of transactions in a block to be committed to the ledger. For the time being only one orderer is needed.
orderers:
  orderer.example.com:
    url: osc-beta.sdsc.edu:7050
    grpcOptions:
      allow-insecure: true

    tlsCACerts:
      path: "/home/fabric/fabric_1.4/fabric-samples/basic-network/crypto-config/ordererOrganizations/example.com/tlsca/tlsca.example.com-cert.pem"

# List of peers to send various requests to, including endorsement, query and event listener registration.
peers:
  peer0.org1.example.com:
    # this URL is used to send endorsement and query requests
#    url: peer0.org1.example.com
    # this URL is used to connect the EventHub and registering event listeners
#    eventUrl: peer0.org1.example.com
    # These parameters should be set in coordination with the keepalive policy on the server
    grpcOptions:
      allow-insecure: true
#      ssl-target-name-override: peer0.org1.example.com
#      grpc.http2.keepalive_time: 15

    tlsCACerts:
      path: "/home/fabric/fabric_1.4/fabric-samples/basic-network/crypto-config/peerOrganizations/org1.example.com/tlsca/tlsca.org1.example.com-cert.pem"

# Fabric-CA is a special kind of Certificate Authority provided by Hyperledger Fabric which allows certificate management to be done via REST APIs.
certificateAuthorities:
  ca.example.com:
    url: http://osc-beta.sdsc.edu:7054
    httpOptions:
      verify: false
    registrar:
      enrollId: admin
      enrollSecret: adminpw
    caName: ca.example.com
    tlsCACerts:
#      path: ${GOPATH}/src/gitlab.com/dimm/osc-portal/hl/basic-network/crypto-config/peerOrganizations/org1.example.com/tlsca/tlsca.org1.example.com-cert.pem
#      path: ${GOPATH}/src/gitlab.com/dimm/osc-portal/hl/basic-network/ca/msp/cacerts/localhost-7054.pem
#path: ${GOPATH}/src/gitlab.com/dimm/osc-portal/hl/ca/client.pem
#      client:
#        key:
#          path: ${GOPATH}/src/gitlab.com/dimm/osc-portal/hl/basic-network/crypto-config/peerOrganizations/org1.example.com/users/User1@org1.example.com/tls/client.key
#        cert:
#          path: ${GOPATH}/src/gitlab.com/dimm/osc-portal/hl/basic-network/crypto-config/peerOrganizations/org1.example.com/users/User1@org1.example.com/tls/client.crt


entityMatchers:
  peer:
    - pattern: (\w*)peer0.org1.example.com(\w*)
      urlSubstitutionExp: osc-beta.sdsc.edu:7051
      eventUrlSubstitutionExp: osc-beta.sdsc.edu:7053
      sslTargetOverrideUrlSubstitutionExp: peer0.org1.example.com
      mappedHost: peer0.org1.example.com

  orderer:
    - pattern: (\w*)orderer.example.com(\w*)
      urlSubstitutionExp: osc-beta.sdsc.edu:7050
      sslTargetOverrideUrlSubstitutionExp: orderer.example.com
      mappedHost: orderer.example.com

  certificateAuthorities:
    - pattern: (\w*)ca.example.com(\w*)
      urlSubstitutionExp: http://osc-beta.sdsc.edu:7054
      mappedHost: ca.example.com
