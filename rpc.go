package main

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"net/http"
	"strings"
	mspclient "github.com/hyperledger/fabric-sdk-go/pkg/client/msp"

)

type PublishServiceSaveArgs struct {
	PublicationId uint
}

type PublishServiceSaveReply struct {
	SignedPubList []Publication
}

type PublishServiceListReply struct {
	PubList []Resource
	SignedPubList []Publication
}

type PublishService struct {}

func registerUser(user User) (string, error) {
	sdk, err := fabsdk.New(config.FromFile(viper.GetString("hl.ConfigFileCA")))

	if err != nil {
		return "", errors.WithMessage(err, "failed to create SDK")
	}

	mspClient, err := mspclient.New(sdk.Context(), mspclient.WithOrg(viper.GetString("hl.OrgName")))
	if err != nil {
		return "", errors.WithMessage(err, "failed to create MSP client")
	}

	userName := user.Subject
	userName = strings.Replace(userName, "://", "-", -1)
	userName = strings.Replace(userName, "/", "-", -1)
	userName = strings.Replace(userName, ".", "-", -1)

	req := mspclient.RegistrationRequest{
		Name:           userName,
		Type:           "user",
		Affiliation: "org1.department1",
		CAName:         viper.GetString("hl.CAName"),
	}

	secret, err := mspClient.Register(&req)
	if err != nil {
		return "", errors.WithMessage(err, "failed to register user")
	}

	mspClient.Enroll(userName, mspclient.WithSecret(secret))

	fmt.Printf("User %s registered and enrolled", user.Subject)

	return secret, nil
}

func InitClient(user User) (*channel.Client, error) {
	sdk, err := fabsdk.New(config.FromFile(viper.GetString("hl.ConfigFile")))

	if err != nil {
		return nil, errors.WithMessage(err, "failed to create SDK")
	}

	userName := user.Subject
	userName = strings.Replace(userName, "://", "-", -1)
	userName = strings.Replace(userName, "/", "-", -1)
	userName = strings.Replace(userName, ".", "-", -1)

	mspClient, err := mspclient.New(sdk.Context(), mspclient.WithOrg(viper.GetString("hl.OrgName")))
	if err != nil {
		return nil, errors.WithMessage(err, "failed to create MSP client")
	}

	userIdentity, err := mspClient.GetSigningIdentity(userName)
	if err != nil {
		fmt.Printf("Error getting signing identity. Registering the user. %s", err.Error())
		if _, err := registerUser(user); err != nil {
			fmt.Printf("Error registering the user. %s", err.Error())
			return nil, errors.WithMessage(err, "failed to register user")
		}
		userIdentity, err = mspClient.GetSigningIdentity(userName)
		if err != nil {
			return nil, errors.WithMessage(err, "failed to get user signing identity")
		}
	}

	clientContext := sdk.ChannelContext(viper.GetString("hl.ChannelID"), fabsdk.WithIdentity(userIdentity))

	client, err := channel.New(clientContext)
	if err != nil {
		return nil, errors.WithMessage(err, "failed to create new channel client")
	}
	fmt.Println("Channel client created")
	return client, nil
}


func (h *PublishService) Save(r *http.Request, args *PublishServiceSaveArgs, reply *PublishServiceSaveReply) error {
	session, err := filestore.Get(r, "osc-session")
	if err != nil {
		return fmt.Errorf("Error getting the session: %s", err.Error())
	}

	var user User
	oscdb.Where("subject = ?", session.Values["id"].(string)).First(&user)

	if client, err := InitClient(user); err != nil {
		return fmt.Errorf("Error init client: %s", err.Error())
	} else {
		var cargs []string
		cargs = append(cargs, "set")
		cargs = append(cargs, "addAsset")

		var resource Resource
		oscdb.Where("user_id = ?", user.ID).Where("id = ?", args.PublicationId).First(&resource)

		publ := Publication{
			resource.Name,
			[]Checksum{
				{
				resource.Type,
				resource.Name,
				resource.Checksum,
				},
			},
			"",
			user.Subject,
			"",
		}

		if b, err := json.Marshal(publ); err == nil {
			response, err := client.Execute(channel.Request{ChaincodeID: viper.GetString("hl.ChainCodeID"), Fcn: cargs[0], Args: [][]byte{[]byte(cargs[1]), b}})
			if err != nil {
				return fmt.Errorf("failed to save data: %s", err.Error())
			}
			fmt.Printf("Save response: %s", string(response.Payload))
			oscdb.Where("user_id = ?", user.ID).Where("id = ?", args.PublicationId).Delete(&resource)



			var pubs []UserPublication
			var signedResources []Publication

			if err := json.Unmarshal(response.Payload, &pubs); err != nil {
				return fmt.Errorf("Failed to unmarshar pubs: %s", err.Error())
			} else {
				for _, upub := range pubs {
					if pubJson, err := base64.StdEncoding.DecodeString(upub.Data); err != nil {
						return fmt.Errorf("Failed to decode pub: %s", err.Error())
					} else {
						var pub Publication
						if err := json.Unmarshal(pubJson, &pub); err != nil {
							return fmt.Errorf("Failed to unmarshal pub: %s", err.Error())
						} else {
							signedResources = append(signedResources, pub)
						}
					}
				}
				reply.SignedPubList = signedResources
			}
		} else {
			return fmt.Errorf("Error saving data: %s", err.Error())
		}

	}

	return nil
}

func (h *PublishService) List(r *http.Request, args *interface{}, reply *PublishServiceListReply) error {
	session, err := filestore.Get(r, "osc-session")
	if err != nil {
		return fmt.Errorf("Error getting the session: %s", err.Error())
	}

	var user User
	oscdb.Where("subject = ?", session.Values["id"].(string)).First(&user)

	var resources []Resource
	var signedResources []Publication

	oscdb.Model(&user).Related(&resources)

	if client, err := InitClient(user); err != nil {
		return fmt.Errorf("Error init client: %s", err.Error())
	} else {
		response, err := client.Query(channel.Request{ChaincodeID: viper.GetString("hl.ChainCodeID"), Fcn: "invoke", Args: [][]byte{[]byte("query")}})
		if err != nil {
			return fmt.Errorf("failed to query: %s", err.Error())
		} else {
			fmt.Printf("Query result: %s", response.Payload)

			var pubs []UserPublication
			if err := json.Unmarshal(response.Payload, &pubs); err != nil {
				return fmt.Errorf("Failed to unmarshar pubs: %s", err.Error())
			} else {
				for _, upub := range pubs {
					if pubJson, err := base64.StdEncoding.DecodeString(upub.Data); err != nil {
						return fmt.Errorf("Failed to decode pub: %s", err.Error())
					} else {
						var pub Publication
						if err := json.Unmarshal(pubJson, &pub); err != nil {
							return fmt.Errorf("Failed to unmarshal pub: %s", err.Error())
						} else {
							signedResources = append(signedResources, pub)
						}
					}

				}

			}

		}

		//fmt.Printf("result: %s", string(response.Payload))
		reply.SignedPubList = signedResources
	}

	reply.PubList = resources

	return nil
}
