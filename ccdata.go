package main

import (
	"bufio"
	"bytes"
	"fmt"
	"strings"
)

type Publication struct {
	Name       string   `json:"name"`
	Checksums		[]Checksum `json:"checksums"`
	DOI       string   `json:"doi"`
	UserID	string   `json:"userid"`
	CreatorID	string   `json:"creatorid"`
}

type Checksum struct{
	Type string `json:"type"`
	Filename string `json:"filename"`
	Checksum string `json:"checksum"`
}

type UserPublication struct {
	Key string `json:"key"`
	Data string `json:"data"`
}

func ParseChecksum(manifest []byte) ([]Checksum, error) {
	var res = []Checksum{}
	scanner := bufio.NewScanner(bytes.NewReader(manifest))
	for scanner.Scan() {
		line := scanner.Text()
		tok := strings.Split(line, " ")
		if len(tok) != 4 ||
			tok[2] != "=" ||
			!strings.HasPrefix(tok[1], "(") ||
			!strings.HasSuffix(tok[1], ")") {
			return res, fmt.Errorf ("Error parsing line %s", line)
		}
		res = append(res, Checksum{tok[0], strings.TrimPrefix(strings.TrimSuffix(tok[1], ")"), "("), tok[3]})
	}
	return res, nil
}