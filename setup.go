package main

import (
	"fmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/channel"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/event"
	"github.com/hyperledger/fabric-sdk-go/pkg/client/resmgmt"
	"github.com/hyperledger/fabric-sdk-go/pkg/common/errors/retry"
	"github.com/hyperledger/fabric-sdk-go/pkg/core/config"
	packager "github.com/hyperledger/fabric-sdk-go/pkg/fab/ccpackager/gopackager"
	"github.com/hyperledger/fabric-sdk-go/pkg/fabsdk"
	"github.com/hyperledger/fabric-sdk-go/third_party/github.com/hyperledger/fabric/common/cauthdsl"
	"github.com/pkg/errors"
)

//packager "github.com/hyperledger/fabric-sdk-go/pkg/fab/ccpackager/gopackager"
//"github.com/hyperledger/fabric-sdk-go/third_party/github.com/hyperledger/fabric/common/cauthdsl")
//mspclient "github.com/hyperledger/fabric-sdk-go/pkg/client/msp"
//"github.com/hyperledger/fabric-sdk-go/pkg/common/providers/msp"


// FabricSetup implementation
type FabricSetup struct {
	ConfigFile      string
	OrgID           string
	OrdererID	string
	ChannelID       string
	ChainCodeID     string
	initialized     bool
	ChannelConfig   string
	ChaincodeGoPath string
	ChaincodePath   string
	ChaincodeVersion string
	OrgAdmin        string
	OrgName         string
	UserName        string
	client          *channel.Client
	event           *event.Client
	admin           *resmgmt.Client
	sdk             *fabsdk.FabricSDK
}

// Initialize reads the configuration file and sets up the client, chain and event hub
func (setup *FabricSetup) Initialize() error {

	// Add parameters for the initialization
	if setup.initialized {
		return errors.New("sdk already initialized")
	}

	// Initialize the SDK with the configuration file
	sdk, err := fabsdk.New(config.FromFile(setup.ConfigFile))

	if err != nil {
		return errors.WithMessage(err, "failed to create SDK")
	}
	setup.sdk = sdk
	fmt.Println("SDK created")

	// The resource management client is responsible for managing channels (create/update channel)
	resourceManagerClientContext := setup.sdk.Context(fabsdk.WithUser(setup.OrgAdmin), fabsdk.WithOrg(setup.OrgName))
	if err != nil {
		return errors.WithMessage(err, "failed to load Admin identity")
	}
	resMgmtClient, err := resmgmt.New(resourceManagerClientContext)
	if err != nil {
		return errors.WithMessage(err, "failed to create channel management client from Admin identity")
	}

	setup.admin = resMgmtClient
	fmt.Println("Ressource management client created")

	//The MSP client allow us to retrieve user information from their identity, like its signing identity which we will need to save the channel
	//mspClient, err := mspclient.New(sdk.Context(), mspclient.WithOrg(setup.OrgName))
	//if err != nil {
	//	return errors.WithMessage(err, "failed to create MSP client")
	//}
	//
	//adminIdentity, err := mspClient.GetSigningIdentity(setup.OrgAdmin)
	//if err != nil {
	//	return errors.WithMessage(err, "failed to get admin signing identity")
	//}

	//req := resmgmt.SaveChannelRequest{ChannelID: setup.ChannelID, ChannelConfigPath: setup.ChannelConfig, SigningIdentities: []msp.SigningIdentity{adminIdentity}}
	//txID, err := setup.admin.SaveChannel(req, resmgmt.WithOrdererEndpoint(setup.OrdererID))
	//if err != nil || txID.TransactionID == "" {
	//	return errors.WithMessage(err, "failed to save channel")
	//}
	//fmt.Println("Channel created")


	//!!!!!!!!!!!!!!!!!!!!!!
	//Make admin user join the previously created channel
	//if err = setup.admin.JoinChannel(setup.ChannelID, resmgmt.WithRetry(retry.DefaultResMgmtOpts), resmgmt.WithOrdererEndpoint(setup.OrdererID)); err != nil {
	//	return errors.WithMessage(err, "failed to make admin join channel")
	//}
	//fmt.Println("Channel joined")

	clientContext := setup.sdk.ChannelContext(setup.ChannelID, fabsdk.WithUser(setup.UserName))
	setup.client, err = channel.New(clientContext)
	if err != nil {
		return errors.WithMessage(err, "failed to create new channel client")
	}
	fmt.Println("Channel client created")

	// Creation of the client which will enables access to our channel events
	setup.event, err = event.New(clientContext)
	if err != nil {
		return errors.WithMessage(err, "failed to create new event client")
	}
	fmt.Println("Event client created")



	fmt.Println("Initialization Successful")
	setup.initialized = true
	return nil
}

func (setup *FabricSetup) InstallAndInstantiateCC() error {

	if ccPkg, err := packager.NewCCPackage(setup.ChaincodePath, setup.ChaincodeGoPath); err != nil {
		return errors.WithMessage(err, "failed to create chaincode package")

	} else {
		fmt.Println("ccPkg created")

		installCCReq := resmgmt.InstallCCRequest{Name: setup.ChainCodeID, Path: setup.ChaincodePath, Version: setup.ChaincodeVersion, Package: ccPkg}
		if _, err = setup.admin.InstallCC(installCCReq, resmgmt.WithRetry(retry.DefaultResMgmtOpts)); err != nil {
			fmt.Printf("failed to install chaincode: %s", err.Error())
		} else {
			ccPolicy := cauthdsl.SignedByAnyMember([]string{"Org1MSP"})

			if(setup.ChaincodeVersion != "0.1") { // if version is not 0.1, doing upgrade, otherwise instantiate. We don't need to instantiate after an upgrade.
				upgradeCCReq := resmgmt.UpgradeCCRequest{Name: setup.ChainCodeID, Path: setup.ChaincodePath, Version: setup.ChaincodeVersion, Policy: ccPolicy}
				if _, err = setup.admin.UpgradeCC(setup.ChannelID, upgradeCCReq, resmgmt.WithRetry(retry.DefaultResMgmtOpts)); err != nil {
					fmt.Printf("failed to upgrade chaincode: %s", err.Error())
				} else {
					fmt.Println("Chaincode Installation & Upgrade Successful")
				}
			} else {
				resp, err := setup.admin.InstantiateCC(setup.ChannelID, resmgmt.InstantiateCCRequest{Name: setup.ChainCodeID, Path: setup.ChaincodeGoPath, Version: setup.ChaincodeVersion, Args: [][]byte{[]byte("init")}, Policy: ccPolicy})
				if err != nil || resp.TransactionID == "" {
					return errors.WithMessage(err, "failed to instantiate the chaincode")
				}
				fmt.Println("Chaincode Installation & Instantiation Successful")
			}
		}
	}

	return nil
}

func (setup *FabricSetup) CloseSDK() {
	setup.sdk.Close()
}