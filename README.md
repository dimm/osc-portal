# OSC portal + chaincode install

```
fabric@osc-beta:~$ cd ~/fabric_1.4/fabric-samples/basic-network/
fabric@osc-beta:~/fabric_1.4/fabric-samples/basic-network$
```

Optional: cleanup

```
fabric@osc-beta:~/fabric_1.4/fabric-samples/basic-network$ ./teardown.sh
fabric@osc-beta:~/fabric_1.4/fabric-samples/basic-network$ docker system prune -af --volumes
```

Start the network:

```
fabric@osc-beta:~/fabric_1.4/fabric-samples/basic-network$ ./init.sh
fabric@osc-beta:~/fabric_1.4/fabric-samples/basic-network$ ./start.sh
fabric@osc-beta:~/fabric_1.4/fabric-samples/basic-network$ docker ps
CONTAINER ID        IMAGE                        COMMAND                  CREATED             STATUS              PORTS                                            NAMES
7029cc41313e        hyperledger/fabric-peer      "peer node start"        24 seconds ago      Up 22 seconds       0.0.0.0:7051->7051/tcp, 0.0.0.0:7053->7053/tcp   peer0.org1.example.com
ac3ea8e63cc7        hyperledger/fabric-ca        "sh -c 'fabric-ca-se…"   26 seconds ago      Up 24 seconds       0.0.0.0:7054->7054/tcp                           ca.example.com
924c7760e2fc        hyperledger/fabric-couchdb   "tini -- /docker-ent…"   26 seconds ago      Up 24 seconds       4369/tcp, 9100/tcp, 0.0.0.0:5984->5984/tcp       couchdb
852bf04dd825        hyperledger/fabric-orderer   "orderer"                27 seconds ago      Up 24 seconds       0.0.0.0:7050->7050/tcp                           orderer.example.com
```


Enroll the CA user
```
fabric@osc-beta:~/fabric_1.4/fabric-samples/basic-network$ docker exec -it ca.example.com bash

root@ac3ea8e63cc7:/# fabric-ca-client enroll -u http://admin:adminpw@localhost:7054 -H /opt/newuser
2019/04/02 23:23:23 [INFO] Created a default configuration file at /opt/newuser/fabric-ca-client-config.yaml
2019/04/02 23:23:23 [INFO] generating key: &{A:ecdsa S:256}
2019/04/02 23:23:23 [INFO] encoded CSR
2019/04/02 23:23:23 [INFO] Stored client certificate at /opt/newuser/msp/signcerts/cert.pem
2019/04/02 23:23:23 [INFO] Stored root CA certificate at /opt/newuser/msp/cacerts/localhost-7054.pem
2019/04/02 23:23:23 [INFO] Stored Issuer public key at /opt/newuser/msp/IssuerPublicKey
2019/04/02 23:23:23 [INFO] Stored Issuer revocation public key at /opt/newuser/msp/IssuerRevocationPublicKey
```

Exit the container.

Get the portal project

```
fabric@osc-beta:~$ cd $GOPATH/src/gitlab.com/dimm/
fabric@osc-beta:~/go/src/gitlab.com/dimm$ git clone git@gitlab.com:dimm/osc-portal.git
fabric@osc-beta:~/go/src/gitlab.com/dimm$ git clone git@gitlab.com:dimm/osc-chaincode.git
fabric@osc-beta:~/go/src/gitlab.com/dimm$ cd osc-portal
fabric@osc-beta:~/go/src/gitlab.com/dimm/osc-portal$ go get ./...
```

Install SDK according to https://github.com/hyperledger/fabric-sdk-go readme

Copy the CA user's keys to ca folder

```
fabric@osc-beta:~$ cd $GOPATH/src/gitlab.com/dimm/osc-portal/hl
fabric@osc-beta:~/go/src/gitlab.com/dimm/osc-portal/hl$ mkdir ca
fabric@osc-beta:~/go/src/gitlab.com/dimm/osc-portal/hl$ cd ca
fabric@osc-beta:~/go/src/gitlab.com/dimm/osc-portal/hl/ca$ docker cp ca.example.com:/opt/newuser/msp .
```

Create a config.toml with right config in project's folder

Install the chaincode

```
fabric@osc-beta:~/go/src/gitlab.com/dimm/osc-portal$ ./osc-portal installcc
 [fabsdk/fab] 2019/04/03 00:29:40 UTC - fab.detectDeprecatedNetworkConfig -> WARN Getting orderers from endpoint config channels.orderer is deprecated, use entity matchers to override orderer configuration
 [fabsdk/fab] 2019/04/03 00:29:40 UTC - fab.detectDeprecatedNetworkConfig -> WARN visit https://github.com/hyperledger/fabric-sdk-go/blob/master/test/fixtures/config/overrides/local_entity_matchers.yaml for samples
SDK created
Ressource management client created
Channel client created
Event client created
Initialization Successful
ccPkg created
Chaincode instantiated
Chaincode Installation & Instantiation Successful
```

Check it's running

```
fabric@osc-beta:~/go/src/gitlab.com/dimm/osc-portal$ docker ps
CONTAINER ID        IMAGE                                                                                                   COMMAND                  CREATED             STATUS              PORTS                                            NAMES
5a17299ba132        dev-peer0.org1.example.com-osccc-0.2-8f455840266ebb7c6ac51bff639a5cbdae8098fa94022af1399b797c95d27812   "chaincode -peer.add…"   23 seconds ago      Up 22 seconds                                                        dev-peer0.org1.example.com-osccc-0.2
7029cc41313e        hyperledger/fabric-peer                                                                                 "peer node start"        About an hour ago   Up About an hour    0.0.0.0:7051->7051/tcp, 0.0.0.0:7053->7053/tcp   peer0.org1.example.com
ac3ea8e63cc7        hyperledger/fabric-ca                                                                                   "sh -c 'fabric-ca-se…"   About an hour ago   Up About an hour    0.0.0.0:7054->7054/tcp                           ca.example.com
924c7760e2fc        hyperledger/fabric-couchdb                                                                              "tini -- /docker-ent…"   About an hour ago   Up About an hour    4369/tcp, 9100/tcp, 0.0.0.0:5984->5984/tcp       couchdb
852bf04dd825        hyperledger/fabric-orderer                                                                              "orderer"                About an hour ago   Up About an hour    0.0.0.0:7050->7050/tcp                           orderer.example.com
```

Run the portal daemon

```
[Unit]
Description=OSC service

[Service]
Type=simple
Restart=always
RestartSec=5s
ExecStart=/home/fabric/go/src/gitlab.com/dimm/osc-portal/osc-portal
WorkingDirectory=/home/fabric/go/src/gitlab.com/dimm/osc-portal
User=fabric
Group=fabric

[Install]
WantedBy=multi-user.target
```

```
systemctl start osc-portal
```

Enjoy the OSC

# Architecture

## Communication

The portal is built in golang. All pages are go templates in `template` folder, rendered dynamically on request. Rendering of resources on the web page is done using Vue.JS library on client side, and talking to portal backend using JsonRPC. There are 2 methods, List and Save, in rpc.go - to list available resources and to save from staging to blockchain. Uploading files or manifests to staging is done using REST.

## Keys storage

When action is performed on the blockchain, the portal tries to get the user's key from key storage. The portal is storing keys in location defined in `hl/config.yaml` `credentialStore` section, and it gets the users private keys when registering a new user. If portal can't get the key, it assumes the user has not been registered yet, and tries to register one, and then perform the action again.
  
## Signing

Since portal is using user's keys for transactions, it's possible to get the key information in chaincode when adding and retrieving data. The chaincode stores the information from the key with the published data in `creatorid` field base64-encoded, e.g. 
`x509::CN=http-cilogon-org-serverA-users-15761,OU=user+OU=org1+OU=department1::CN=ca.org1.example.com,O=org1.example.com,L=San Francisco,ST=California,C=US`

This makes sure we always know which key was used to make the modification.

The CIlogon IDs used to identify users is being transformed to the form `http://cilogon.org/serverA/users/1111` -> `http-cilogon-org-serverA-users-1111` to avoid problems with creating new users (seems like escaping is not done right in the blockchain rest service)