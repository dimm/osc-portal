package main

import "github.com/jinzhu/gorm"

type Resource struct {
	gorm.Model
	UserID uint
	Name   string
	Checksum string
	Type string
}

// Not used
type Cert struct {
	gorm.Model
	UserID uint
	Certificate   string
	Key   string
}

type User struct {
	gorm.Model
	Email  string  `gorm:"unique_index;not null"`
	Subject  string  `gorm:"unique_index;not null"`
	Name string
	GroupID uint
	Resources []Resource
	Cert Cert // Not used
}

// Not used
type Group struct {
	gorm.Model
	Name string
	Users []User
}

func initDB() error {
	db_, err := gorm.Open("sqlite3", "users.db")
	oscdb = db_
	if err != nil {
		return err
	}
	//oscdb.LogMode(true)
	oscdb.Exec("PRAGMA foreign_keys = ON")


	oscdb.AutoMigrate(&User{})
	oscdb.AutoMigrate(&Group{})

	oscdb.Model(&User{}).Related(&[]Group{})

	oscdb.AutoMigrate(&Resource{})

	oscdb.Model(&User{}).Related(&Cert{})
	oscdb.AutoMigrate(&Cert{})

	oscdb.Model(&Resource{}).Related(&User{})

	return nil
}