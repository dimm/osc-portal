require.config({
    baseUrl: 'static',
    paths:{
        'vue': '//cdnjs.cloudflare.com/ajax/libs/vue/2.6.8/vue.min',
        'vex': '//cdnjs.cloudflare.com/ajax/libs/vex-js/4.1.0/js/vex.combined.min',
        'dayjs': '//cdnjs.cloudflare.com/ajax/libs/dayjs/1.8.8/dayjs.min',
        'dayjsrel': '//cdnjs.cloudflare.com/ajax/libs/dayjs/1.8.8/plugin/relativeTime',
    }
});


require(["vue", "vex", "dayjs", "dayjsrel"], function(Vue, vex, dayjs, dayjsrel) {
    vex.defaultOptions.className = 'vex-theme-os';
    dayjs.extend(dayjsrel);

    var certsComp = new Vue({
        el: 'pubs',
        data: function () {
            return {
                pubs: [],
                signedPubs: [],
                errormsg: "",
            };
        },
        mounted: function() {
            var that = this;
            $.ajax({
                type: "POST",
                dataType:"json",
                contentType:"application/json; charset=utf-8",
                url: "/rpc",
                data: JSON.stringify ({
                    "jsonrpc": "2.0",
                    "method": "publish.List",
                    "params": {
                    },
                    "id": "jrpc"
                }),
                success: function (data) {
                    if(data.error){
                        that.errormsg = data.error.message;
                        return;
                    }
                    if(data.result.PubList)
                        that.pubs = data.result.PubList;
                    if(data.result.SignedPubList)
                        that.signedPubs = data.result.SignedPubList;
                },
                error: function (xhr, status, error) {
                    console.log('Error occured ' + xhr.responseText);
                }
            });
        },
        filters: {
            truncate: function (text, length, suffix) {
                if(text.length < length) return text;
                return text.substring(0, length) + suffix;
            },
        },
        methods: {
            formatDay: function(epoch) {
                return dayjs(epoch*1000).fromNow();
            },
            sign: function(pubId) {
                var that = this;
                $.ajax({
                    type: "POST",
                    dataType:"json",
                    contentType:"application/json; charset=utf-8",
                    url: "/rpc",
                    data: JSON.stringify ({
                        "jsonrpc": "2.0",
                        "method": "publish.Save",
                        "params": {
                            "PublicationId": pubId,
                        },
                        "id": "jrpc"
                    }),
                    success: function (data) {
                        if(data["error"]){
                            vex.dialog.alert({ unsafeMessage: 'Error submitting:<br/>'+data.error.message })
                        } else {
                            for(var i=0; i<that.pubs.length; i++){
                                if(that.pubs[i].ID == pubId) {
                                    var rm = that.pubs.splice(i, 1)[0];
                                    break;
                                }
                            }
                            if(data.result.SignedPubList)
                                that.signedPubs = data.result.SignedPubList;
                        }
                    },
                    error: function (xhr, status, error) {
                        vex.dialog.alert('Error submitting:<br/>'+xhr.responseText)
                    }
                });
            }
        },
        template: `
            <div>
                <div v-if="errormsg">{{errormsg}}</div>
                <br/>                
                <h5 v-if="pubs">You have the following pubs:</h5>
                <table v-if="pubs">
                    <tr>
                        <th>Date</th>
                        <th>Name</th>
                        <th>Checksum</th>
                        <th>Checksum type</th>
                        <th></th>
                    </tr>
                    <tr v-for="pub in pubs">
                        <td>{{pub.CreatedAt}}</td>
                        <td>{{pub.Name}}</td>
                        <td>{{pub.Checksum | truncate(30, '...')}}</td>
                        <td>{{pub.Type}}</td>
                        <td><input type="button" @click="sign(pub.ID)" value="Sign"></td>
                    </tr>
                </table>
                <h5 v-else>You don't have any pubs created yet.</h5>

                <br/><br/>                
                <h5 v-if="signedPubs">You have the following signed pubs:</h5>
                <table v-if="signedPubs">
                    <tr>
                        <th>Name</th>
                        <th>Checksums</th>
                        <th>User</th>
                    </tr>
                    <tr v-for="pub in signedPubs">
                        <td>{{pub.name}}</td>
                        <td>
                            <table>
                                <tr v-for="cs in pub.checksums">
                                    <td>{{cs.filename}}</td>    
                                    <td>{{cs.checksum | truncate(30, '...')}}</td>    
                                </tr>
                            </table>    
                        </td>
                        <td>{{pub.userid}}</td>
                    </tr>
                </table>
                <h5 v-else>You don't have any signed pubs created yet.</h5>
            </div>
        `
    });



});
