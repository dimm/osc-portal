package main

import (
	"context"
	"fmt"
	"html/template"
	"log"
	"math/rand"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/coreos/go-oidc"
	"github.com/gorilla/rpc/v2"
	"github.com/gorilla/rpc/v2/json2"
	"github.com/gorilla/sessions"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/spf13/viper"
	"golang.org/x/oauth2"
)

type OIDC_Provider struct {
	Provider *oidc.Provider
	Config   oauth2.Config
}

var oscdb *gorm.DB

var oidc_providers = map[string]OIDC_Provider{}

var filestore *sessions.FilesystemStore

var states = map[string]string{}
var statesLock = sync.RWMutex{}
var ctx = context.Background()

var dataFolder = "data"

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

type IndexData struct {
	User    User
}

func CreateIndexData(session *sessions.Session) IndexData {
	var data IndexData
	if !session.IsNew {
		var user User
		oscdb.Where("subject = ?", session.Values["id"].(string)).First(&user)
		data = IndexData{user}
	}
	return data
}

func randStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	if viper.GetBool("debug") {
		session, err := filestore.Get(r, "osc-session")
		if err != nil {
			log.Printf("Error getting the session: %s", err.Error())
		}
		session.Values["email"] = "debug@debug.com"
		session.Values["id"] = "Subj"
		if e := session.Save(r, w); e != nil {
			http.Error(w, "Failed to save session: "+e.Error(), http.StatusInternalServerError)
			return
		}
		log.Printf("Saved session for debug")

		var user User
		oscdb.FirstOrCreate(&user, User{Email: session.Values["email"].(string), Subject: session.Values["id"].(string)})

		http.Redirect(w, r, "/", http.StatusFound)
		return
	}


	statesLock.Lock()
	defer statesLock.Unlock()

	provider := r.FormValue("provider")
	if provider == "" {
		http.Error(w, "Wrong provider", http.StatusBadRequest)
		return
	}

	newState := randStringBytes(36)
	states[newState] = provider
	cleanStateTimer := time.NewTimer(time.Minute * 10)
	go func() {
		<-cleanStateTimer.C
		statesLock.Lock()
		defer statesLock.Unlock()
		delete(states, newState)
	}()
	config := oidc_providers[provider].Config
	log.Printf("Got redirect: %s", config.AuthCodeURL(newState))
	http.Redirect(w, r, config.AuthCodeURL(newState), http.StatusFound)
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	session, err := filestore.Get(r, "osc-session")
	if err != nil {
		log.Printf("Error getting the session: %s", err.Error())
	}
	t, _ := template.ParseFiles("template/main_layout.tmpl", "template/home.tmpl")
	indexData := CreateIndexData(session)
	t.Execute(w, indexData)
}

func logoutHandler(w http.ResponseWriter, r *http.Request) {
	session, err := filestore.Get(r, "osc-session")
	if err != nil {
		log.Printf("Error getting the session: %s", err.Error())
	}
	session.Options.MaxAge = -1
	if e := session.Save(r, w); e != nil {
		http.Error(w, "Failed to save session: "+e.Error(), http.StatusInternalServerError)
		return
	}
	http.Redirect(w, r, "/", http.StatusFound)
}

func callbackHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		return
	}

	session, err := filestore.Get(r, "osc-session")
	if err != nil {
		log.Printf("Error getting the session: %s", err.Error())
	}

	var provider string
	handleState := func() {
		statesLock.Lock()
		defer statesLock.Unlock()

		_provider, ok := states[r.URL.Query().Get("state")]
		if !ok {
			http.Error(w, "state did not match", http.StatusBadRequest)
			return
		}
		provider = _provider
	}
	handleState()

	config := oidc_providers[provider].Config
	oauth2Token, err := config.Exchange(ctx, r.URL.Query().Get("code"))
	if err != nil {
		http.Error(w, "Failed to exchange token: "+err.Error(), http.StatusInternalServerError)
		return
	}

	userInfo, err := oidc_providers[provider].Provider.UserInfo(ctx, oauth2.StaticTokenSource(oauth2Token))
	if err != nil {
		http.Error(w, "Failed to get userinfo: "+err.Error(), http.StatusInternalServerError)
		return
	}

	session.Values["email"] = userInfo.Email
	session.Values["id"] = userInfo.Subject
	if e := session.Save(r, w); e != nil {
		http.Error(w, "Failed to save session: "+e.Error(), http.StatusInternalServerError)
		return
	}
	log.Printf("Saved session for %s", userInfo.Email)

	var user User
	oscdb.FirstOrCreate(&user, User{Email: session.Values["email"].(string), Subject: session.Values["id"].(string), Name: userInfo.Profile})
	http.Redirect(w, r, "/", http.StatusFound)
}

func initProviders() {
	provider_names := []string{"cilogon"}

	for _, provider_name := range provider_names {
		var provider *oidc.Provider
		var endpoint = oauth2.Endpoint{}
		provider, err := oidc.NewProvider(ctx, viper.Sub(provider_name).GetString("url"))
		if err != nil {
			log.Fatal(err)
		}
		endpoint = provider.Endpoint()
		config := oauth2.Config{
			ClientID:     viper.Sub(provider_name).GetString("client"),
			ClientSecret: viper.Sub(provider_name).GetString("client_secret"),
			Endpoint:     endpoint,
			RedirectURL:  viper.GetString("redirectURL"),
			Scopes:       []string{oidc.ScopeOpenID, "profile", "email"},
		}

		oidc_providers[provider_name] = OIDC_Provider{
			Provider: provider,
			Config:   config,
		}
	}
}

func main() {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("fatal error config file: %s", err))
	}

	if len(os.Args) > 1 {
		fSetup := FabricSetup{
			// Network parameters
			OrdererID: viper.GetString("hl.OrdererID"),

			// Channel parameters
			ChannelID:     viper.GetString("hl.ChannelID"),

			// Chaincode parameters
			ChainCodeID:      viper.GetString("hl.ChainCodeID"),
			ChaincodeGoPath:  viper.GetString("hl.ChaincodeGoPath"),
			ChaincodePath:    viper.GetString("hl.ChaincodePath"),
			ChaincodeVersion: viper.GetString("hl.ChaincodeVersion"),
			OrgAdmin:         viper.GetString("hl.OrgAdmin"),
			OrgName:          viper.GetString("hl.OrgName"),
			ConfigFile:       viper.GetString("hl.ConfigFile"),

			// User parameters
			UserName: viper.GetString("hl.UserName"),
		}

		//Initialization of the Fabric SDK from the previously set properties
		err := fSetup.Initialize()
		if err != nil {
			fmt.Printf("Unable to initialize the Fabric SDK: %v\n", err)
			return
		}

		defer fSetup.CloseSDK()


		switch os.Args[1] {
		case "installcc":
			err = fSetup.InstallAndInstantiateCC()
			if err != nil {
				fmt.Printf("Unable to install and instantiate the chaincode: %v\n", err)
				return
			}
		}
		return
	}

	initProviders()

	os.Mkdir(dataFolder, 0777)

	os.Mkdir("sessions", 0777)
	filestore = sessions.NewFilesystemStore("sessions", []byte(viper.GetString("sessionAuthKey")), []byte(viper.GetString("sessionEncKey")))
	filestore.Options = &sessions.Options{
		Path:     "/",
		MaxAge:   3600 * 24 * 7,
		HttpOnly: true,
	}

	if err := initDB(); err != nil {
		fmt.Printf("Error initing DB: %s", err.Error())
		return
	}
	defer oscdb.Close()

	if viper.IsSet("cookieDomain") {
		filestore.Options.Domain = viper.GetString("cookieDomain")
		if !viper.GetBool("debug") {
			filestore.Options.Secure = true
		}
	}

	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/login", loginHandler)
	http.HandleFunc("/logout", logoutHandler)

	http.HandleFunc("/oidc-callback", callbackHandler)

	http.HandleFunc("/resources", resourcesHandler)
	http.HandleFunc("/upload", uploadHandler)
	http.HandleFunc("/uploadfile", uploadFileHandler)

	s := rpc.NewServer()
	s.RegisterCodec(json2.NewCodec(), "application/json")
	s.RegisterService(new(PublishService), "publish")
	http.Handle("/rpc", s)

	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	http.ListenAndServe("127.0.0.1:5002", nil)
}
